#!/bin/python3

from time import sleep

from rdflib import Graph, Literal, RDF, URIRef
from SPARQLWrapper import SPARQLWrapper, XML


ENDPOINT_LABS = "https://data.labs.pdok.nl/sparql"
ENDPOINT_PROD = "https://data.pdok.nl/sparql"
ENDPOINT_DBPEDIA = "http://dbpedia.org/sparql/?"
TARGET_PROVINCES = ["Drenthe",
                    "Flevoland",
                    "Friesland",
                    "Gelderland",
                    "Groningen",
                    "Limburg",
                    "Noord-Brabant",
                    "Noord-Holland",
                    "Overijssel",
                    "Utrecht",
                    "Zeeland",
                    "Zuid-Holland"]

# 5768 monuments (5527 with point geometry)
#SELECT (COUNT(?monuments) AS ?monumentsCount)
#WHERE {
#      ?monuments rdf:type dbo:Monument ;
#                 dbo:province "Utrecht" ;
#                 geo:hasGeometry _:geop .
#      }

def get_provinces():
    sparql = SPARQLWrapper(ENDPOINT_DBPEDIA)
    query = """
            PREFIX dbo: <http://dbpedia.org/ontology/>
            PREFIX dbr: <http://dbpedia.org/resource/>
            PREFIX foaf: <http://xmlns.com/foaf/0.1/>

            SELECT ?provname
            WHERE {
                ?prov dbo:type dbr:Provinces_of_the_Netherlands ;
                      foaf:name ?provname .
            }
            """
    
    sparql.setQuery(query)
    sparql.setReturnFormat(XML)
    
    return sparql.query().convert() 

def construct_monument_graph_alt():
    sparql = SPARQLWrapper(ENDPOINT_LABS)
    query = """
            PREFIX foaf: <http://xmlns.com/foaf/0.1/>
            PREFIX rce: <https://cultureelerfgoed.nl/vocab/>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

            CONSTRUCT {
                ?monument ?p ?o ;
                          foaf:depiction ?image .
                ?o ?q ?r .
                ?image foaf:depicts ?monument

            }
            WHERE {
                ?monument rdf:type rce:Monument ;
                           ?p ?o .
                ?o ?q ?r .

                ?image foaf:depicts ?monument .
                }
            """

    sparql.setQuery(query)
    sparql.setReturnFormat(XML)

    return sparql.query().convert() 

def construct_monument_graph(province):
    sparql = SPARQLWrapper(ENDPOINT_LABS)
    query = """
            PREFIX dbo: <http://dbpedia.org/ontology/>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

            CONSTRUCT {{
                ?monuments ?p ?o .
                ?o ?q ?r .
            }}
            WHERE {{
                ?monuments rdf:type dbo:Monument ;
                           dbo:province "{}" ;
                           ?p ?o .
                ?o ?q ?r .
                }}
            """.format(province)

    sparql.setQuery(query)
    sparql.setReturnFormat(XML)

    return sparql.query().convert() 

def construct_BAG_graph(g_mon):
    max_ntries = 3
    monuments = set(g_mon.subjects(RDF.type, URIRef("http://dbpedia.org/ontology/Monument")))
    
    g = Graph()
    failed = []
    monument = monuments.pop()
    tries = max_ntries
    while len(monuments) > 0:
        try:
            postalcode = g_mon.value(monument,
                                     URIRef('http://www.w3.org/2006/vcard/ns#postal-code'))
            homenumber = g_mon.value(monument,
                                    URIRef('https://data.pdok.nl/monumenten/vocab/huisnummer'))

            if postalcode is None or homenumber is None:
                continue

            postalcode = Literal(postalcode.value.replace(" ", ""))
            
            # retrieve geometry
            sparql = SPARQLWrapper(ENDPOINT_PROD)
            query = """
                        PREFIX bag: <http://bag.basisregistraties.overheid.nl/def/bag#>         
                        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                        PREFIX geo: <http://www.opengis.net/ont/geosparql#>

                        CONSTRUCT {{  
                            <{}> geo:hasGeometry ?pand_geo .
                            ?pand_geo ?gp ?go .
                          }}
                        WHERE {{
                          {{
                          _:nummeraanduiding bag:postcode "{}" ;
                                             bag:huisnummer "{}"^^xsd:integer .
                          _:verblijfsobject (bag:hoofdadres|bag:nevenadres) _:nummeraanduiding ;
                                                bag:pandrelatering/bag:geometriePand ?pand_geo .
                            ?pand_geo ?gp ?go .
                          }} 
                        }}
                    """.format(monument.toPython(),
                               postalcode.toPython(),
                               homenumber.toPython())

            sparql.setQuery(query)
            sparql.setReturnFormat(XML)

            g += sparql.query().convert() 

            # dont kill the triple store
            sleep(0.2)
            
            monument = monuments.pop()
            if tries != max_ntries:
                tries = max_ntries
        except:
            if tries <= 0:
                failed.append(monument)
            
                sleep(0.2)
                monument = monuments.pop()
                tries = max_ntries
            else:
                tries -= 1
                sleep(120)

    return (g, failed)

def construct_beeldbank_graph(g_mon):
    max_ntries = 3
    monuments = set(list(g_mon.subjects(RDF.type,
                                        URIRef("http://dbpedia.org/ontology/Monument")))[:20])
    
    g = Graph()
    failed = []
    monument = monuments.pop()
    tries = max_ntries
    while len(monuments) > 0:
        try:
            monument_alt = URIRef("https://cultureelerfgoed.nl/id/monument/{}".format(monument.split("/")[-1]))

            # retrieve geometry
            sparql = SPARQLWrapper(ENDPOINT_LABS)
            query = """
                PREFIX foaf: <http://xmlns.com/foaf/0.1/>

                CONSTRUCT {{
                ?image foaf:depicts <{1}> ;
                         ?ip ?io .
                }}
                WHERE {{
                  ?image foaf:depicts <{0}> ;
                         ?ip ?io .
                }}""".format(monument.toPython(),
                             monument_alt.toPython())

            sparql.setQuery(query)
            sparql.setReturnFormat(XML)

            g += sparql.query().convert() 

            # dont kill the triple store
            sleep(0.2)
            
            monument = monuments.pop()
            if tries != max_ntries:
                tries = max_ntries
        except:
            if tries <= 0:
                failed.append(monument)
            
                sleep(0.2)
                monument = monuments.pop()
                tries = max_ntries
            else:
                tries -= 1
                sleep(120)

    return (g, failed)


def stats_of(g):
    stats = {}

    stats['ntriples'] = len(g)

    monuments = set(g.subjects(RDF.type, URIRef("http://dbpedia.org/ontology/Monument")))
    m = set(mon for mon in monuments if (mon,
                                    URIRef("http://www.opengis.net/ont/geosparql#hasGeometry"),
                                    None) in g)
    monuments_alt = set(g.objects(None,
                                  URIRef("http://xmlns.com/foaf/0.1/depicts")))
    #monuments_complete = 

    stats['nmonuments'] = len(set(g.subjects(RDF.type,
                                             URIRef("http://dbpedia.org/ontology/Monument"))))
    stats['ngeometries'] = len(set(g.subjects(RDF.type,
                                              URIRef("http://www.opengis.net/ont/geosparql#Geometry"))))
    stats['nimages'] = len(set(g.subjects(RDF.type,
                                          URIRef("https://data.pdok.nl/beeldbank/vocab/Image"))))

    results = g.query("""
                PREFIX dbo: <http://dbpedia.org/ontology/>
              
                SELECT (count(distinct ?mon) as ?nmonuments)
                WHERE {
                        ?mon a dbo:Monument .
                       }
              """)
   
    results = g.query("""
                PREFIX dbo: <http://dbpedia.org/ontology/>
                PREFIX geo: <http://www.opengis.net/ont/geosparql#>
              
                SELECT (count(distinct ?mon) as  ?nmonuments)
                WHERE {
                        ?mon a dbo:Monument ;
                         geo:hasGeometry _:geom .

                       }
              """)
   
    results = g.query("""
                PREFIX bldbnk: <https://data.pdok.nl/beeldbank/vocab/>
                PREFIX dbo: <http://dbpedia.org/ontology/>
                PREFIX foaf: <http://xmlns.com/foaf/0.1/>
                PREFIX owl: <http://www.w3.org/2002/07/owl#> 
              
                SELECT (count(distinct ?mon) as  ?nmonuments)
                WHERE {
                        ?mon a dbo:Monument ;
                         owl:sameAs ?monbldbnk .
                       
                        ?img a bldbnk:Image ;
                              foaf:depicts ?monbldbnk .
                       }
              """)
                     
   results = g.query("""
                PREFIX bldbnk: <https://data.pdok.nl/beeldbank/vocab/>
                PREFIX dbo: <http://dbpedia.org/ontology/>
                PREFIX foaf: <http://xmlns.com/foaf/0.1/>
                PREFIX geo: <http://www.opengis.net/ont/geosparql#>
                PREFIX owl: <http://www.w3.org/2002/07/owl#> 
              
                SELECT (count(distinct ?mon) as  ?nmonuments)
                WHERE {
                        ?mon a dbo:Monument ;
                          geo:hasGeometry _:geom ;
                          owl:sameAs ?monbldbnk .

                        _:img a bldbnk:Image ;
                              foaf:depicts ?monbldbnk .
                       }
              """)

def main():
    g = Graph()

    print("Retrieving monuments...", end="")
    for province in TARGET_PROVINCES:
        print("- {}".format(province))
        g += construct_monument_graph(province)
        sleep(1)
    
    print("\t\tdone ({} triples, {} monuments)".format(len(g), len(monuments)))
    

    print("Retrieving monument geometries...", end="")
    g_geo = construct_BAG_graph(g)
    print("\t\tdone ({} triples)".format(len(g_geo)))

    print("Retrieving monument images...", end="")
    g_img = convert_beeldbank_graph()
    print("\t\tdone ({} triples)".format(len(g_img)))

    print("Integrating graphs...", end="")
    g += g_mon
    g += g_geo
    g += g_img
    print("\t\tdone")

    print("Serializing graph...", end="")
    g.serialize(destination='./dataset.ttl', format='ttl')
    print("\t\tdone")
